# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
# Translators:
# Дмитрий Ошкало <dmitry.oshkalo@gmail.com>, 2019
# Kашицин Роман <calorus@gmail.com>, 2019
# Олег o1hk <o1h2k3@yandex.ru>, 2019
# AlexL <loginov.alex.valer@gmail.com>, 2019
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-28 09:00+0300\n"
"PO-Revision-Date: 2019-05-09 00:48+0000\n"
"Last-Translator: AlexL <loginov.alex.valer@gmail.com>, 2019\n"
"Language-Team: Russian (https://www.transifex.com/rus-open-source/teams/44267/ru/)\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);\n"

#: app/examples/Misc/Explorer/.project:21
msgid "File browser example"
msgstr "Пример браузера файлов"

#: app/examples/Misc/Explorer/.project:22
msgid "IconView example"
msgstr "Пример IconView"

#: app/examples/Misc/Explorer/.src/FExplorer.class:153
msgid ""
"IconView example written by\n"
"Benoît Minisini"
msgstr ""
"Пример просмотра значков, написанный\n"
"Бенуа Минисини"

#: app/examples/Misc/Explorer/.src/FExplorer.class:159
msgid "' has been renamed to '"
msgstr "' был переименован в '"

#: app/examples/Misc/Explorer/.src/FExplorer.form:5
msgid "FExplorer"
msgstr "Обзорщик"

#: app/examples/Misc/Explorer/.src/FExplorer.form:9
msgid "File"
msgstr "Файл"

#: app/examples/Misc/Explorer/.src/FExplorer.form:11
msgid "View hidden files"
msgstr "Просмотреть скрытые файлы"

#: app/examples/Misc/Explorer/.src/FExplorer.form:14
msgid "Refresh"
msgstr "Освежить"

#: app/examples/Misc/Explorer/.src/FExplorer.form:21
msgid "About"
msgstr "О программе"

#: app/examples/Misc/Explorer/.src/FExplorer.form:24
msgid "Quit"
msgstr "Выход"

