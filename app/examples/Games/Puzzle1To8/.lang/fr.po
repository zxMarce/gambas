#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Puzzle1To8 3.13.90\n"
"POT-Creation-Date: 2019-06-04 19:28 UTC\n"
"PO-Revision-Date: 2019-06-04 19:25 UTC\n"
"Last-Translator: benoit <benoit@benoit-kubuntu>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "Puzzle 1 to 8"
msgstr "Puzzle 1 à 8"

#: .project:2 FrmAyuda.form:19
msgid "Place the numbers 1 to 8 in each of the boxes without repeating, so that each box does not contain a correlative number to its neighbors or adjacent boxes. See the following examples:"
msgstr "Placez les nombres 1 à 8 dans chacune des cases sans vous répéter, de sorte que chaque boîte ne contienne pas un nombre corrélatif à ses voisins ou à des cases adjacentes. Voir les exemples suivants :"

#: FMain.class:69
msgid "Are you sure?"
msgstr "Es-tu sûr ?"

#: FMain.class:69
msgid "No"
msgstr "Non"

#: FMain.class:69
msgid "Yes"
msgstr "Oui"

#: FMain.class:90
msgid "Congratulations! You did it!"
msgstr "Félicitations ! Vous avez réussi !"

#: FMain.form:22
msgid "Puzzle 1 to 8 - Locate the numbers!"
msgstr "Puzzle 1 à 8 - Trouvez les chiffres !"

#: FMain.form:27
msgid "Game"
msgstr "Jeu"

#: FMain.form:30
msgid "Clear"
msgstr "Effacer"

#: FMain.form:35
msgid "Quit"
msgstr "Quitter"

#: FMain.form:41
msgid "Help"
msgstr "Aide"

#: FMain.form:44 FrmAyuda.form:14
msgid "How to play?"
msgstr "Comment jouer ?"

#: FMain.form:49 FrmAbout.form:12
msgid "About"
msgstr "A propos"

#: FrmAbout.form:21
msgid "Author"
msgstr "Auteur"

#: FrmAbout.form:25
msgid ""
"Author: Pablo Mileti.<br> <br>You can submit your questions, suggestions, bugs, etc, to the following E-Mail address:\n"
"<a href=\"mailto:pablomileti@gmail.com\"> pablomileti@gmail.com</a>"
msgstr ""
"Auteur: Pablo Mileti.<br><br>Vous pouvez soumettre vos questions, suggestions, bogues, etc. à l’adresse E-Mail suivante :\n"
"<a href=\"\"mailto:pablomileti@gmail.com\"\">pablomileti@gmail.com</a>"

#: FrmAbout.form:29
msgid "License"
msgstr "Licence"

#: FrmAbout.form:33
msgid ""
"\n"
"    Copyright (C) 2010. Author: Pablo Mileti  \n"
"\n"
"This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.\n"
"\n"
"This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.\n"
"\n"
"You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>."
msgstr ""
"\n"
"Copyright (C) 2010. Auteur: Pablo Mileti\n"
"\n"
"Ce programme est un logiciel libre: vous pouvez le redistribuer et/ou le modifier selon les termes de la licence publique générale GNU telle que publiée par la Free Software Foundation, soit la version 3 de la licence, soit (à votre discrétion) toute version ultérieure.\n"
"\n"
"Ce programme est distribué dans l’espoir qu’il sera utile, mais sans aucune garantie; sans même la garantie implicite de qualité marchande ou d’adéquation à un usage particulier.  Pour plus de détails, consultez la licence publique générale GNU.\n"
"\n"
"Vous auriez dû recevoir une copie de la licence publique générale GNU ainsi que ce programme.  Sinon, voyez <http: www.gnu.org/licenses/=\"\">.</http:>"

#: FrmAbout.form:43 FrmAyuda.form:43
msgid "Close"
msgstr "Fermer"

#: FrmAyuda.form:31
msgid "<b>Way incorrect:</b><br><br>The numbers 1 and 2 are consecutive and are in adjoining boxes, so it is not allowed."
msgstr "<b>Manière incorrecte:</b><br><br>Les nombres 1 et 2 sont consécutifs et sont dans des cases adjacentes, il n’est donc pas autorisé."

#: FrmAyuda.form:48
msgid "<b>Way correct:</b><br><br>There aren't correlativity between to the numbers entered with respect to their adjoining boxes, so it's allowed."
msgstr "<b>Façon correcte:</b><br><br>Il n’y a pas de corrélation entre les nombres entrés par rapport à leurs cases adjacentes, donc c’est permis."
